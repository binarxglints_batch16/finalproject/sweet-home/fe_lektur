export { default as PenIcons } from './icons/svg/PenIcons.jsx';
export { default as PaperPlane } from './icons/svg/PaperPlane.jsx';
export { default as CupOfCoffee } from './icons/svg/CupOfCoffee.jsx';
export { ReactComponent as HomeIcons } from './icons/HomeIcons.svg';
export { ReactComponent as HeartIcons } from './icons/svg/Heart.svg';
export { ReactComponent as NoHeartIcons } from './icons/svg/Icon - Default.svg';
export { ReactComponent as Liked } from './icons/svg/Liked.svg';
