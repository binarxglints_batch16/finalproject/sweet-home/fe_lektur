import { Form } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import {
  setSectionsFilter,
  setStylesFilter,
} from '../../redux/actionCreators/filterAction';

const FilterCheckbox = (props) => {
  const data = props.data;
  const title = props.title;
  const dispatch = useDispatch();

  const handleCheckbox = (e, idx, arr) => {
    const checked = e.target.checked;
    switch (title) {
      case 'Sections':
        return dispatch(setSectionsFilter({ checked, idx, arr }));
      case 'Styles':
        return dispatch(setStylesFilter({ checked, idx, arr }));
      default:
        break;
    }
  };

  return (
    <div className='FilterCheckbox'>
      <Form className='bg-cloud p-3 mb-4'>
        <h5 className='fw-bold fs-6 mb-3'>{title}</h5>
        {data.map((element, idx, arr) => (
          <Form.Check
            key={idx}
            type='checkbox'
            checked={element.value}
            value={element.name}
            id={`${title}-${element.name}`}
            label={element.name}
            onChange={(e) => handleCheckbox(e, idx, arr)}
          />
        ))}
      </Form>
    </div>
  );
};

export default FilterCheckbox;
