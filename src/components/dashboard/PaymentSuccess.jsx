import moment from 'moment';

const PaymentSuccess = (props)=>{
   return(
      <div className="receipt">
         <div className="text-ash">Receipt</div>
            <div className="fw-bold text-primary text-truncate">{props.confirm}</div>
            <div className="text-ash">
               <span className="mb-0 pe-1">{`Upload at ${moment(props.complete).format('LL')}`}</span>
            </div>
      </div>
  );
};

export default PaymentSuccess;
