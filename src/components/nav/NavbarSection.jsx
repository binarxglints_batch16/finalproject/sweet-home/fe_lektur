import { Container, Dropdown, Nav, Navbar } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import Logo from "../../assets/icons/svg/SweetHome.svg";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Login, Register } from "../../components";
import { useLoginModal } from "../../context/LoginContext";
import { getUser } from "../../redux/actionCreators/userAction";
// React Context
// Custom Hooks

const NavbarSection = (props) => {
  const background = props.background;

  const { showLoginModal, setShowLoginModal } = useLoginModal();
  //   const [showLogin, setShowLogin] = useState(false);
  const [showRegister, setShowRegister] = useState(false);
  const token = localStorage.getItem("token");
  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const signOut = () => {
    localStorage.removeItem("token");
    window.location.reload();
  };

  const direct = () => {
    navigate("/dashboard");
  };

  useEffect(() => {
    dispatch(getUser());
    // eslint-disable-next-line
  }, []);

  return (
    <div className="NavbarSection">
      <Navbar
        collapseOnSelect
        expand="md"
        bg={background && "white"}
        fixed="top"
        variant="light"
      >
        <Container fluid>
          <Navbar.Brand as={Link} to="/" className="p-2">
            <img src={Logo} alt="logo SweetHome" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link href="#whatWeDo" className="fw-bold">
                Our Service
              </Nav.Link>
              <Nav.Link as={Link} to="/showcase" className="fw-bold">
                Showcase
              </Nav.Link>
            </Nav>
            {token ? (
              <>
                {user.loading ? (
                  <div className="greeting text-primary fw-bold">
                    Hello Sweetie
                  </div>
                ) : (
                  <div className="wrapper-profile ms-2">
                    <img
                      className="rounded-circle"
                      src={
                        user.data.picture ||
                        "https://www.adinata.org/content/about/structure/1_1620680872_user.png"
                      }
                      alt="profile"
                    />
                    <Dropdown className="bg-transparent border-0 shadow-none ">
                      <Dropdown.Toggle
                        className="bg-transparent p-0 border-0"
                        id="dropdown-basic"
                      >
                        <span className="text-ash fw-bold ms-2 profile-name">
                          {`${user.data.firstName} ${user.data.lastName}`}
                        </span>
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item onClick={() => direct()}>
                          Dashboard
                        </Dropdown.Item>
                        <Dropdown.Item onClick={signOut}>
                          Sign Out
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                )}
              </>
            ) : (
              <Nav>
                <Nav.Link
                  onClick={(e) => {
                    e.preventDefault();
                    setShowLoginModal(true);
                  }}
                  as={Link}
                  to="/login"
                  className="fw-bold text-secondary"
                >
                  Login
                </Nav.Link>
                <Nav.Link
                  onClick={(e) => {
                    e.preventDefault();
                    setShowRegister(true);
                  }}
                  as={Link}
                  to="/signup"
                  className="fw-bold text-secondary"
                >
                  Sign Up
                </Nav.Link>
              </Nav>
            )}
          </Navbar.Collapse>
        </Container>
        <Login show={showLoginModal} onHide={() => setShowLoginModal(false)} />
        {/* <Login show={showLogin} onHide={() => setShowLogin(false)} /> */}
        <Register show={showRegister} onHide={() => setShowRegister(false)} />
      </Navbar>

      {/* {showLogin && <LoginForm onClose={() => {
        setShowLogin(false)
      }} />} */}
      {/* <Routes>
        <Route path="/login" element={<LoginForm />} />
      </Routes>
       */}
    </div>
  );
};

export default NavbarSection;
