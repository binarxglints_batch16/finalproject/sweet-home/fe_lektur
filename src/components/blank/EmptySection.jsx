import React from "react";
import Picture from "../../assets/icons/svg/Empty.svg";

const EmptySection = (props) => {
  return (
    <div className="wrapper-empty">
      <div className="empty-icon text-center pb-5">
        <img className="picture-empty w-25" src={Picture} alt="noData" />
        <h1 className="quote fw-bold text-secondary">
          Ooooppsss,
        </h1>
        <h5 className="quote fw-bold text-primary">
          {`You don't have any ${props.quote} yet`}
        </h5>
      </div>
    </div>
  );
};

export default EmptySection;
