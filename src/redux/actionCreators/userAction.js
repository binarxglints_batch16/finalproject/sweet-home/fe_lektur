import { SET_USER } from '../constants';

import axios from 'axios';

export const getUser = () => {
  return (dispatch) => {
    dispatch({
      type: SET_USER,
      payload: {
        loading: true,
        data: false,
        error: false,
      },
    });
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_BASE_API}user/`,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    })
      .then((response) => {
        dispatch({
          type: SET_USER,
          payload: {
            loading: false,
            data: response.data.result[0],
            error: false,
          },
        });
      })
      .catch((error) => {
        dispatch({
          type: SET_USER,
          payload: {
            loading: false,
            data: false,
            error: error.message,
          },
        });
      });
  };
};
