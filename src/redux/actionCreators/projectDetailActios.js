import {
  GET_PROJECT_DETAIL_REQUEST,
  GET_PROJECT_DETAIL_SUCCES,
  GET_PROJECT_DETAIL_ERROR,
} from '../constants';
import axios from 'axios';

export const getProjectDetail = (id) => {
  return (dispatch) => {
    dispatch({
      type: GET_PROJECT_DETAIL_REQUEST,
      payload: {
        project: false,
        loading: true,
        error: false,
      },
    });
    axios({
       method: 'get',
       url:`${process.env.REACT_APP_BASE_API}showcase/${id}`,
    })
      .then((response) => {
      //   console.log(response);
        dispatch({
          type: GET_PROJECT_DETAIL_SUCCES,
          payload: {
            project: response.data.data,
            loading: false,
            error: false,
          },
        });
      })
      .catch((error) => {
        dispatch({
          type: GET_PROJECT_DETAIL_ERROR,
          payload: {
            project: false,
            loading: false,
            error: error.message,
          },
        });
      });
  };
};
